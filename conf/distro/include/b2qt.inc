############################################################################
##
## Copyright (C) 2021 The Qt Company Ltd.
## Contact: https://www.qt.io/licensing/
##
## This file is part of the Boot to Qt meta layer.
##
## $QT_BEGIN_LICENSE:GPL$
## Commercial License Usage
## Licensees holding valid commercial Qt licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and The Qt Company. For licensing terms
## and conditions see https://www.qt.io/terms-conditions. For further
## information use the contact form at https://www.qt.io/contact-us.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3 or (at your option) any later version
## approved by the KDE Free Qt Foundation. The licenses are as published by
## the Free Software Foundation and appearing in the file LICENSE.GPL3
## included in the packaging of this file. Please review the following
## information to ensure the GNU General Public License requirements will
## be met: https://www.gnu.org/licenses/gpl-3.0.html.
##
## $QT_END_LICENSE$
##
############################################################################

DEPLOY_CONF_NAME_stm32mp1-eval  = "STM32MP15 Evaluation"
DEPLOY_CONF_NAME_stm32mp1-disco = "STM32MP15 Discovery"

ACCEPT_EULA_stm32mp1-eval   = "1"
ACCEPT_EULA_stm32mp1-disco  = "1"

DISTRO_FEATURES_remove = " vulkan wayland "

IMAGE_FSTYPES_remove = "tar.xz"

# Define ROOTFS_MAXSIZE to 3GB
IMAGE_ROOTFS_MAXSIZE = "3145728"

# ogl-runtime depends on gles3
OGL_RUNTIME = ""
OGL_RUNTIME_DEV = ""

ST_VENDORFS = "0"

IMAGE_INSTALL_append = " resize-helper systemd-mount-partitions userfs-cleanup-package "

TOOLCHAIN_HOST_TASK_remove_sdkmingw32 = " ${ST_TOOLS_FOR_SDK} "
TOOLCHAIN_HOST_TASK_remove_sdkmingw32 = " ${ST_DEPENDENCIES_BUILD_FOR_SDK} "

BBMASK += "\
    meta-st-openstlinux/recipes-qt \
    meta-st-openstlinux/recipes-connectivity \
    meta-st-openstlinux/recipes-graphics \
    meta-st-openstlinux/recipes-multimedia \
    meta-st-openstlinux/recipes-webadmin \
    meta-st-openstlinux/oe-core \
    meta-st-openstlinux/recipes-st \
    meta-st-openstlinux/recipes-samples \
    meta-st-openstlinux/recipes-core/systemd/systemd_244.3.bbappend \
"

